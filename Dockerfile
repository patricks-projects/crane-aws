ARG DOCKER_REGISTRY_IMAGE_PREFIX
FROM ${DOCKER_REGISTRY_IMAGE_PREFIX}amazonlinux:2

RUN amazon-linux-extras install kernel-ng epel golang1.11

ENV GOPATH=$HOME/go
ENV PATH=$PATH:$GOPATH/bin
ENV GO111MODULE=on

RUN yum -y install gcc autoconf automake libtool make libudev-devel kernel-headers systemd-devel libgcrypt-devel rpm-devel file file-devel libcap-ng-devel libseccomp-devel lmdb-devel python3-devel jq unzip curl rpm-build unzip uthash-devel

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" &&\
    unzip awscliv2.zip &&\
    ./aws/install &&\
    rm awscliv2.zip

RUN go install github.com/google/go-containerregistry/cmd/crane@latest